'use strict';

var _cluster = require('cluster');

var _cluster2 = _interopRequireDefault(_cluster);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

if (_cluster2.default.isMaster) {
    // Count the machine's CPUs
    var cpuCount = require('os').cpus().length;

    // Create a worker for each CPU
    for (var i = 0; i < cpuCount; i += 1) {
        _cluster2.default.fork();
    }

    // Listen for dying workers
    _cluster2.default.on('exit', function () {
        _cluster2.default.fork();
    });
} else {
    require('./index');
}
//# sourceMappingURL=cluster.js.map