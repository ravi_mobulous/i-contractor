'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
const TOKEN_SECRET = exports.TOKEN_SECRET = 'i-contractor';
const ACCESS_TOKEN_EXPIRY = exports.ACCESS_TOKEN_EXPIRY = 60 * 60 * 24 * 7;
const ACCESS_TOKEN_ALGO = exports.ACCESS_TOKEN_ALGO = 'HS256';
const PASS_HASH_ROUNDS = exports.PASS_HASH_ROUNDS = 10;
const EMAIL_HOST = exports.EMAIL_HOST = 'smtp.mail.us-east-1.awsapps.com';
const EMAIL_PORT = exports.EMAIL_PORT = 465;
const EMAIL_SECURE = exports.EMAIL_SECURE = true;
const EMAIL_USERNAME = exports.EMAIL_USERNAME = 'test@mobulous.com';
const EMAIL_PASSWORD = exports.EMAIL_PASSWORD = 'Test@2k2020';
//# sourceMappingURL=index.js.map