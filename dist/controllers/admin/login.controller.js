'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.adminChangePassword = exports.loginAdmin = undefined;

var _bcrypt = require('bcrypt');

var _bcrypt2 = _interopRequireDefault(_bcrypt);

var _requestValidators = require('./_requestValidators');

var _sendResponse = require('../../utils/sendResponse');

var _jwt = require('../../utils/jwt');

var _config = require('../../config');

var _models = require('../../models');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const loginAdmin = exports.loginAdmin = async (req, res) => {
    try {
        const errors = (0, _requestValidators.validateLoginPayload)(req);
        if (errors) {
            return (0, _sendResponse.sendResponse)(res, 404, {}, errors[0].msg);
        }

        let bodyData = req.body;
        let userCheck = await _models.Admin.findOne({ email: bodyData.email.toLowerCase() });
        if (!userCheck) {
            return (0, _sendResponse.sendResponse)(res, 400, {}, 'Invalid Credential, user not found');
        }
        if (!(await _bcrypt2.default.compare(bodyData.password, userCheck.password))) {
            return (0, _sendResponse.sendResponse)(res, 400, {}, 'Invalid Credential, user not found');
        }
        if (userCheck.status !== '1') {
            return (0, _sendResponse.sendResponse)(res, 400, {}, "User is inactive");
        }
        userCheck.adminToken = await (0, _jwt.issueJWT)(bodyData.email);
        await userCheck.save();
        (0, _sendResponse.sendResponse)(res, 200, userCheck, "Admin Login Successfully");
    } catch (error) {
        return (0, _sendResponse.handleCustomThrow)(res, error);
    }
};

const adminChangePassword = exports.adminChangePassword = async (req, res) => {
    try {
        const errors = (0, _requestValidators.validateAdminChangePasswordPayload)(req);
        if (errors) {
            return (0, _sendResponse.sendResponse)(res, 404, {}, errors[0].msg);
        }
        let bodyData = req.body;
        if (!(await _bcrypt2.default.compare(bodyData.oldPassword, req.user.password))) {
            return (0, _sendResponse.sendResponse)(res, 400, {}, 'Wrong old password');
        }
        const salt = _bcrypt2.default.genSaltSync(_config.PASS_HASH_ROUNDS);
        const hash = _bcrypt2.default.hashSync(bodyData.newPassword, salt);

        (0, _sendResponse.sendResponse)(res, 200, {}, "Password update successfully");

        await _models.Admin.updateOne({ _id: req.user._id }, { password: hash });
    } catch (error) {
        return (0, _sendResponse.handleCustomThrow)(res, error);
    }
};
//# sourceMappingURL=login.controller.js.map