'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.userStatusManage = exports.userDetails = exports.userList = undefined;

var _requestValidators = require('./_requestValidators');

var _sendResponse = require('../../utils/sendResponse');

var _models = require('../../models');

const userList = exports.userList = async (req, res) => {
    try {
        const errors = (0, _requestValidators.validateUserListPayload)(req);
        if (errors) {
            return (0, _sendResponse.sendResponse)(res, 404, {}, errors[0].msg);
        }
        let match;
        let query = req.query;
        let limit = 10;
        let skip = query.page ? +query.page - 1 : 0;
        switch (query.list) {
            case 'all':
                match = { "$match": {} };
                break;
            case 'approved':
                match = { "$match": { adminApproved: '1' } };
                break;
            case 'pending':
                match = { "$match": { adminApproved: '0' } };
                break;
            case 'blocked':
                match = { "$match": { status: '0' } };
                break;
        }
        let user = await _models.User.aggregate([{
            "$facet": {
                "totalData": [match, { "$skip": skip * limit }, { "$limit": limit }],
                "totalCount": [match, {
                    "$group": {
                        "_id": null,
                        "count": { "$sum": 1 }
                    }
                }]
            }
        }]);
        (0, _sendResponse.sendResponse)(res, 200, user, "User List");
    } catch (error) {
        return (0, _sendResponse.handleCustomThrow)(res, error);
    }
};

const userDetails = exports.userDetails = async (req, res) => {
    try {
        const errors = (0, _requestValidators.validateUserDetailsPayload)(req);
        if (errors) {
            return (0, _sendResponse.sendResponse)(res, 404, {}, errors[0].msg);
        }
        let result = {};
        result.userData = await _models.User.findOne({ _id: req.query.userId }).populate('carModel');
        result.youthData = await _models.Youth.find({ userId: req.query.userId });
        (0, _sendResponse.sendResponse)(res, 200, result, "User Details");
    } catch (error) {
        return (0, _sendResponse.handleCustomThrow)(res, error);
    }
};

const userStatusManage = exports.userStatusManage = async (req, res) => {
    try {
        const errors = (0, _requestValidators.validateUserStatusManagePayload)(req);
        if (errors) {
            return (0, _sendResponse.sendResponse)(res, 404, {}, errors[0].msg);
        }

        await _models.User.findOneAndUpdate({ _id: req.query.userId }, req.body);
        (0, _sendResponse.sendResponse)(res, 200, {}, "User updated successfully");
    } catch (error) {
        return (0, _sendResponse.handleCustomThrow)(res, error);
    }
};
//# sourceMappingURL=user.controller.js.map