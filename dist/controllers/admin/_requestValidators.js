'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.validateCreateCarModePayload = validateCreateCarModePayload;
exports.validateLoginPayload = validateLoginPayload;
exports.validateAdminChangePasswordPayload = validateAdminChangePasswordPayload;
exports.validateUserListPayload = validateUserListPayload;
exports.validateUserDetailsPayload = validateUserDetailsPayload;
exports.validateUserStatusManagePayload = validateUserStatusManagePayload;
exports.validateRideDetails = validateRideDetails;

var _db = require('../../db');

var _db2 = _interopRequireDefault(_db);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const isValidObjectId = value => _db2.default.Types.ObjectId.isValid(value);

function validateCreateCarModePayload(req) {
    req.checkBody('name', 'name is required/invalid').notEmpty();
    return req.validationErrors();
}
function validateLoginPayload(req) {
    req.checkBody('email', 'Email is required/invalid').isEmail().exists();
    req.checkBody('password', 'password is required/invalid').notEmpty();
    return req.validationErrors();
}
function validateAdminChangePasswordPayload(req) {
    req.checkBody('oldPassword', 'oldPassword is required/invalid').notEmpty();
    req.checkBody('newPassword', 'newPassword is required/invalid').notEmpty();
    return req.validationErrors();
}

function validateUserListPayload(req) {
    req.checkQuery('list', 'list is required/invalid').isIn(['all', 'approved', 'pending', 'blocked']);
    return req.validationErrors();
}
function validateUserDetailsPayload(req) {
    req.checkQuery('userId', 'userId is required/invalid').custom(isValidObjectId);
    return req.validationErrors();
}
function validateUserStatusManagePayload(req) {
    req.checkQuery('userId', 'userId is required/invalid').custom(isValidObjectId);
    return req.validationErrors();
}
function validateRideDetails(req) {
    req.checkQuery('rideId', 'rideId is required/invalid').custom(isValidObjectId);
    return req.validationErrors();
}
//# sourceMappingURL=_requestValidators.js.map