'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _login = require('./login.controller');

Object.keys(_login).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _login[key];
    }
  });
});

var _user = require('./user.controller');

Object.keys(_user).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _user[key];
    }
  });
});
//# sourceMappingURL=index.js.map