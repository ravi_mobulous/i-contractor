'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.validateLogin = validateLogin;
exports.validateOtpVerify = validateOtpVerify;

var _db = require('../../db');

var _db2 = _interopRequireDefault(_db);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const isValidObjectId = value => _db2.default.Types.ObjectId.isValid(value);

function validateLogin(req) {
    req.checkBody('email', 'Email is required/invalid').isEmail().exists();
    req.checkBody('name', 'name is required/invalid').notEmpty();
    req.checkBody('deviceType', 'Device Type is required/invalid').isIn(['android', 'ios']);
    req.checkBody('deviceToken', 'Device Token is required/invalid').notEmpty();
    return req.validationErrors();
}
function validateOtpVerify(req) {
    req.checkBody('email', 'Email is required/invalid').isEmail().exists();
    req.checkBody('otp', 'otp is required/invalid').notEmpty();
    return req.validationErrors();
}
//# sourceMappingURL=_requestValidators.js.map