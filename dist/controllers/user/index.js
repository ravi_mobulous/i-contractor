'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _userAuth = require('./userAuth.controller');

Object.keys(_userAuth).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _userAuth[key];
    }
  });
});
//# sourceMappingURL=index.js.map