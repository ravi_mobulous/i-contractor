'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.verifyOtp = exports.authUser = undefined;

var _ejs = require('ejs');

var _ejs2 = _interopRequireDefault(_ejs);

var _requestValidators = require('./_requestValidators');

var _sendResponse = require('../../utils/sendResponse');

var _jwt = require('../../utils/jwt');

var _email = require('../../utils/email');

var _models = require('../../models');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const authUser = exports.authUser = async (req, res, next) => {
    try {
        const errors = (0, _requestValidators.validateLogin)(req);
        if (errors) {
            return (0, _sendResponse.sendResponse)(res, 400, {}, errors[0].msg);
        }
        let body = req.body;
        let randomNumber = Math.floor(1000 + Math.random() * 9000);

        body.otpData = {
            otp: randomNumber,
            createdAt: new Date()
        };

        if (req.body.lat && req.body.long) {
            body.location = {
                type: 'Point',
                coordinates: [body.long, body.lat]
            };
        }
        let user = await _models.User.findOneAndUpdate({ email: body.email }, body, { upsert: true, setDefaultsOnInsert: true });

        (0, _sendResponse.sendResponse)(res, 200, user || {}, "Otp send to the email. Please verify.");

        let emailPageData = { name: body.name, randomNumber };
        let html = await _ejs2.default.renderFile(__basedir + "/views/user-otp.ejs", emailPageData);
        //  send mail to user 
        let mailData = {
            to: body.email,
            subject: "I-Contractor OTP",
            html
        };
        await (0, _email.sendMail)(mailData);
    } catch (error) {
        next(error);
    }
};

const verifyOtp = exports.verifyOtp = async (req, res, next) => {
    try {
        const errors = (0, _requestValidators.validateOtpVerify)(req);
        if (errors) {
            return (0, _sendResponse.sendResponse)(res, 400, {}, errors[0].msg);
        }
        let email = req.body.email;
        // get user by email
        let user = await _models.User.findOne({ email });

        if (!user) {
            return (0, _sendResponse.sendResponse)(res, 400, {}, "Email not found");
        }

        // compair otp form database
        if (user.otpData.otp != req.body.otp) {
            return (0, _sendResponse.sendResponse)(res, 400, {}, "OTP doesnot match");
        }

        // check otp is expired or not 
        let expireTime = new Date(user.otpData.createdAt).getTime() + 600000;
        let currentTime = new Date().getTime();

        if (expireTime < currentTime) {
            return (0, _sendResponse.sendResponse)(res, 400, {}, "OTP is expired");
        }

        // issue JWT token for the user and save to database 
        user.userToken = await (0, _jwt.issueJWT)({ payload: { email } });
        user.otpData.otp = null;
        user.save();

        (0, _sendResponse.sendResponse)(res, 200, user, 'OTP verify successfully');
    } catch (error) {
        next(error);
    }
};
//# sourceMappingURL=userAuth.controller.js.map