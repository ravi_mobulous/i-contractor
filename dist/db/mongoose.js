'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_mongoose2.default.Promise = global.Promise;

const MONGODB_URI = 'mongodb://127.0.0.1:27017/i-contractor';

const options = {
    useUnifiedTopology: true,
    useNewUrlParser: true
};

_mongoose2.default.connect(MONGODB_URI, options);
_mongoose2.default.set('useFindAndModify', false);
_mongoose2.default.set('useCreateIndex', true);

_mongoose2.default.connection.on('connected', () => {
    // eslint-disable-next-line no-console
    console.info(`Connected to MongoDB`);
});
_mongoose2.default.connection.on('error', err => {
    // eslint-disable-next-line no-console
    console.error(`MongoDB connection error:`, err);
    process.exit(-1);
});

_mongoose2.default.connection.on('disconnected', () => {
    // eslint-disable-next-line no-console
    console.error('MongoDB disconnected');
});

exports.default = _mongoose2.default;
//# sourceMappingURL=mongoose.js.map