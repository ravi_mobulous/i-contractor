'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isAdminAuthenticated = exports.isAuthenticated = undefined;

var _isAuthenticated = require('./isAuthenticated');

var _isAuthenticated2 = _interopRequireDefault(_isAuthenticated);

var _isAdminAuthenticated = require('./isAdminAuthenticated');

var _isAdminAuthenticated2 = _interopRequireDefault(_isAdminAuthenticated);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.isAuthenticated = _isAuthenticated2.default;
exports.isAdminAuthenticated = _isAdminAuthenticated2.default;
//# sourceMappingURL=index.js.map