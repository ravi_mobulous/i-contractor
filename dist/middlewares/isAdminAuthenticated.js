'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _sendResponse = require('../utils/sendResponse');

var _jwt = require('../utils/jwt');

var _models = require('../models');

const INVALID_TOKEN = 'Invalid/Missing token';

const isAdminAuthenticated = async (req, res, next) => {
    // check req.header for token
    try {
        const token = req.header('Authorization');
        if (!token) {
            return (0, _sendResponse.sendResponse)(res, 401, {}, INVALID_TOKEN);
        }
        const decoded = await (0, _jwt.decodeJWT)({ token });

        if (!decoded) {
            return (0, _sendResponse.sendResponse)(res, 401, {}, INVALID_TOKEN);
        }
        if (!decoded.data) {
            return (0, _sendResponse.sendResponse)(res, 401, {}, INVALID_TOKEN);
        }
        const user = await _models.Admin.findOne({ adminToken: token });
        if (!user) {
            return (0, _sendResponse.sendResponse)(res, 401, {}, INVALID_TOKEN);
        }
        req.user = user;

        return next();
    } catch (err) {
        // eslint-disable-next-line no-console
        console.error(err);
        if (err.name === 'TokenExpiredError') {
            return (0, _sendResponse.sendResponse)(res, 401, {}, 'Token expired');
        } else if (err.name === 'JsonWebTokenError') {
            return (0, _sendResponse.sendResponse)(res, 401, {}, INVALID_TOKEN);
        }
        return (0, _sendResponse.sendResponse)(res, 500, {}, 'Something went wrong');
    }
};

exports.default = isAdminAuthenticated;
//# sourceMappingURL=isAdminAuthenticated.js.map