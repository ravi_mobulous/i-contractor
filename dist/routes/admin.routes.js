'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _middlewares = require('../middlewares');

var _admin = require('../controllers/admin');

var admin = _interopRequireWildcard(_admin);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

const userRoutes = (0, _express.Router)();
userRoutes.post('/loginAdmin', admin.loginAdmin);
userRoutes.post('/changePassword', _middlewares.isAdminAuthenticated, admin.adminChangePassword);

exports.default = userRoutes;
//# sourceMappingURL=admin.routes.js.map