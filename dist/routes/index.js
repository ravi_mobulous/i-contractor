'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _user = require('./user.routes');

var _user2 = _interopRequireDefault(_user);

var _admin = require('./admin.routes');

var _admin2 = _interopRequireDefault(_admin);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const routes = (0, _express.Router)();

routes.use('/', _user2.default);
routes.use('/admin', _admin2.default);
exports.default = routes;
//# sourceMappingURL=index.js.map