'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _middlewares = require('../middlewares');

var _user = require('./../controllers/user');

var user = _interopRequireWildcard(_user);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

const userRoutes = (0, _express.Router)();

userRoutes.post('/authUser', user.authUser);
userRoutes.post('/verifyOtp', user.verifyOtp);

exports.default = userRoutes;
//# sourceMappingURL=user.routes.js.map