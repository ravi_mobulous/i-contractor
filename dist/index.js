'use strict';

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _cors = require('cors');

var _cors2 = _interopRequireDefault(_cors);

var _helmet = require('helmet');

var _helmet2 = _interopRequireDefault(_helmet);

var _expressValidator = require('express-validator');

var _expressValidator2 = _interopRequireDefault(_expressValidator);

var _sendResponse = require('./utils/sendResponse');

var _routes = require('./routes');

var _routes2 = _interopRequireDefault(_routes);

require('./db');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const app = (0, _express2.default)();


app.disable('x-powered-by');
app.use((0, _helmet2.default)());
app.use(_bodyParser2.default.json());
app.use(_bodyParser2.default.urlencoded({ extended: false }));
app.use((0, _cors2.default)());
app.use((0, _expressValidator2.default)({
    customValidators: {
        isDate: value => !isNaN(Date.parse(value))
    }
}));

app.use((req, res, next) => {
    console.log({
        URL: req.url,
        body: req.body
    });
    next();
});

// Routes
app.use('/', _routes2.default);

// Catch 404 and forward to error handler
app.use((err, req, res, next) => {
    (0, _sendResponse.handleCustomThrow)(res, err);
});

const { PORT = 4000 } = process.env;
app.listen(PORT, () => console.log(`Listening on port ${PORT}`)); //
//# sourceMappingURL=index.js.map