'use strict';

var _db = require('../db');

var _db2 = _interopRequireDefault(_db);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const NotificationSchema = new _db2.default.Schema({
    userId: {
        type: _db2.default.Schema.Types.ObjectId,
        ref: 'Users',
        required: true
    },
    id: {
        type: String,
        trim: true,
        index: true
    },
    title: {
        type: String,
        trim: true,
        index: true,
        required: true
    },
    body: {
        type: String,
        trim: true,
        index: true,
        required: true
    },
    type: {
        type: String,
        trim: true,
        index: true,
        required: true
    },
    status: {
        type: String,
        enum: ['0', '1'],
        default: '0'
    }
}, { timestamps: true });

module.exports = _db2.default.model('Notifications', NotificationSchema);
//# sourceMappingURL=Notification.model.js.map