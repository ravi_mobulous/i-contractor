'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Notification = exports.Admin = exports.User = undefined;

var _User = require('./User.model');

var _User2 = _interopRequireDefault(_User);

var _Admin = require('./Admin.model');

var _Admin2 = _interopRequireDefault(_Admin);

var _Notification = require('./Notification.model');

var _Notification2 = _interopRequireDefault(_Notification);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.User = _User2.default;
exports.Admin = _Admin2.default;
exports.Notification = _Notification2.default;
//# sourceMappingURL=index.js.map