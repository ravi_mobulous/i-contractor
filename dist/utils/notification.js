"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.pushNotify = undefined;

var _firebaseAdmin = require("firebase-admin");

var _firebaseAdmin2 = _interopRequireDefault(_firebaseAdmin);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var serviceAccount = require("./petparenting-1136d-firebase-adminsdk-651za-74c7a93d85.json");

_firebaseAdmin2.default.initializeApp({
    credential: _firebaseAdmin2.default.credential.cert(serviceAccount),
    databaseURL: "https://petparenting-1136d.firebaseio.com"
});

const pushNotify = exports.pushNotify = ({ data, token }) => {

    let message = {
        data,
        notification: {
            title: data.title,
            body: data.body
        },
        token
    };
    _firebaseAdmin2.default.messaging().send(message).then(response => {
        console.log('Notification sent :', response);
    }).catch(error => {
        console.log('Error Notification', error);
    });
};
//# sourceMappingURL=notification.js.map