'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.sendMail = undefined;

var _nodemailer = require('nodemailer');

var _nodemailer2 = _interopRequireDefault(_nodemailer);

var _index = require('../config/index');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const transporter = _nodemailer2.default.createTransport({
    host: _index.EMAIL_HOST,
    port: _index.EMAIL_PORT,
    secure: _index.EMAIL_SECURE, // use SSL
    auth: {
        user: _index.EMAIL_USERNAME,
        pass: _index.EMAIL_PASSWORD
    }
});

const sendMail = exports.sendMail = ({ to, subject, html }) => {
    let mailOptions = {
        from: _index.EMAIL_USERNAME,
        to,
        subject,
        html
    };

    return transporter.sendMail(mailOptions);
};
//# sourceMappingURL=email.js.map