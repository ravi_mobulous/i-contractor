'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.issueJWT = issueJWT;
exports.generateTemporaryTokens = generateTemporaryTokens;
exports.verifyJWT = verifyJWT;
exports.decodeJWT = decodeJWT;

var _jsonwebtoken = require('jsonwebtoken');

var _jsonwebtoken2 = _interopRequireDefault(_jsonwebtoken);

var _v = require('uuid/v4');

var _v2 = _interopRequireDefault(_v);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _config = require('../config');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

if (!_config.TOKEN_SECRET || !_config.ACCESS_TOKEN_EXPIRY || !_config.ACCESS_TOKEN_ALGO) {
    throw new Error('JWT settings not found in env');
}

async function issueJWT(phone) {
    const token = await _jsonwebtoken2.default.sign({
        exp: _config.ACCESS_TOKEN_EXPIRY,
        algorithm: _config.ACCESS_TOKEN_ALGO,
        data: phone
    }, _config.TOKEN_SECRET);
    return token;
}

async function generateTemporaryTokens({ activeDays }) {
    const token = (0, _v2.default)();
    const expiry = (0, _moment2.default)().add(activeDays, 'd').valueOf();
    return {
        token,
        expiry
    };
}

async function verifyJWT({ token }) {
    const isValid = await _jsonwebtoken2.default.verify(token, _config.TOKEN_SECRET);
    return isValid;
}

async function decodeJWT({ token }) {
    const decoded = await _jsonwebtoken2.default.decode(token, _config.TOKEN_SECRET, {
        expiresIn: _config.ACCESS_TOKEN_EXPIRY,
        algorithm: _config.ACCESS_TOKEN_ALGO
    });
    return decoded;
}
//# sourceMappingURL=jwt.js.map